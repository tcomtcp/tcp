TCP
===

Install GTK+2.0
---------------

* `apt-get install libgtk2.0-dev`
* `apt-get install libgtk2.0-0`

or use ./misc/libs.sh

How to print
------------

Include `utils.h` and then use one of those two functions:

* `P("string")` will display for the server: `[SRV] string\n`
* `PF("string %d", 1)` will display for the server: `[SRV] string 1\n`

Those macros will automatically display `[CLT]` or `[SRV]` in their output, depending on the binary they're called from.
