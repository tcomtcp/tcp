#include <tcp/structs.h>
#include <arpa/inet.h>
#include <stdlib.h>

int check_ip_port(char *ip, char *port, s_authority *auth)
{
    struct sockaddr_in sa;

    if (ip != NULL)
    {
        if (inet_pton(AF_INET, ip, &sa.sin_addr) <= 0)
            return 0; // Error: Invalid IP address
        else if (auth != NULL)
            auth->ip = sa.sin_addr.s_addr;
    }
    else if (auth != NULL)
        auth->ip = INADDR_ANY;

    int pval = atoi(port);
    if (pval < 1 || pval > 65535)
        return 0; // Error: Invalid port
    else if (auth != NULL)
        auth->port = pval;

    return 1; // Success
}
