#ifndef CONN_CONN_H
# define CONN_CONN_H

# include <user/context.h>

# define MAX_PAYLOAD (2048)

void conn_loop(s_context *context);

#endif // !CONN_CONN_H
