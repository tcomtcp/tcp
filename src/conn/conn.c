#include <conn/conn.h>
#include <conn/input.h>
#include <conn/send.h>
#include <tcp/tcb.h>
#include <user/ui.h>
#include <user/print.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <signal.h>
#include <pthread.h>
#include <poll.h>

static void signal_handler(int sig __attribute__((unused)))
{
    #ifdef DEBUG
        P("Connection thread killed");
    #endif
    int ret = 0;
    pthread_exit(&ret);
}

static s_sock_conn *set_conn(s_authority *auth)
{
    s_sock_conn *c = malloc(sizeof (s_sock_conn));

    if ((c->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        P("ERROR: SOCKET NOT SET");
        c->sock = 0;
    }

    memset((char *)&c->si, 0, sizeof (c->si));
    c->si.sin_family = AF_INET;
    c->si.sin_port = htons(auth->port);
    c->si.sin_addr.s_addr = auth->ip;

    return c;
}

void conn_loop(s_context *context)
{
    signal(SIGUSR1, signal_handler);

    s_tcb *tcb = context->tcb;

#ifdef IS_SERVER
    s_authority *auth = &tcb->local_auth;
#else
    s_authority *auth = &tcb->remote_auth;
#endif

    check_ip_port(context->field_ip, context->field_port, auth); // Has already been checked in UI

    tcb->conn = set_conn(auth);
    s_sock_conn *c = tcb->conn;
    socklen_t slen = sizeof (c->si);
    char buf[sizeof (s_tcp_header) + MAX_PAYLOAD];

#ifdef IS_SERVER
    if (bind(c->sock, (struct sockaddr*)&c->si, sizeof (c->si)) == -1)
    {
        P("ERROR: could not bind socket for listen");
        return;
    }
# ifdef DEBUG
    else
        PF("SUCCESS: binded socket, will now listen on port %d...", c->si.sin_port);
# endif
    tcb->state = LISTEN;
#else
    // HACK: Sending an empty paquet to get a port assigned :D (don't tell my parents)
    sendto(tcb->conn->sock, 0, 0, 0, (struct sockaddr *)&tcb->conn->si, sizeof (tcb->conn->si));
    struct sockaddr_in local_addr;
    socklen_t addr_len = sizeof (local_addr);
    getsockname(tcb->conn->sock, (struct sockaddr *)&local_addr, &addr_len);
    tcb->local_auth.port = ntohs(local_addr.sin_port);

    set_header(tcb, SYN);
    tcb->head_list->head->segment->header.seq = atoi(context->field_seq);
    send_segment(tcb->conn, tcb->head_list->head->segment); // FIXME: get info from ui before sending, but how to handle this state?

    print_header(tcb);
    tcb->state = SYN_SENT;
#endif
    ui_display_header(1); // Update to show LISTEN for server. SYN_SENT state for client will be lightening fast

    struct pollfd fds[2];
    long nfds = sizeof (fds) / sizeof (struct pollfd);
    fds[0].fd = c->sock;
    fds[0].events = POLLIN;
    fds[1].fd = context->ui_event_fd;
    fds[1].events = POLLIN;

    while (1)
    {
        if (poll(fds, nfds, 0) > 0)
        {
            for (int i = 0; i < nfds; ++i)
            {
                if (fds[i].fd == c->sock && fds[i].revents & POLLIN)
                {
                    if (recvfrom(c->sock, buf, sizeof (s_tcp_header) + MAX_PAYLOAD, 0, (struct sockaddr *)&c->si, &slen) == -1)
                    {
                        P("ERROR: could not receive data");
                    }
                    else
                    {
                        #ifdef DEBUG
                            P("Received data");
                        #endif
                        s_segment *seg = init_segment();
                        add_ll(tcb->rcv_head_list, seg);
                        memcpy(&seg->header, buf, sizeof (s_tcp_header));
                        if (seg->header.payload_length > 0)
                        {
                            tcb->cur_window += seg->header.payload_length;

                            size_t pos = tcb->rcv_file_sz;
                            tcb->rcv_file_sz += seg->header.payload_length;
                            if (pos == 0)
                                tcb->rcv_file = malloc(tcb->rcv_file_sz);
                            else
                                tcb->rcv_file = realloc(tcb->rcv_file, tcb->rcv_file_sz);

                            memcpy(tcb->rcv_file + pos, buf + sizeof (s_tcp_header), seg->header.payload_length);

                            // OSI L5 shit
                            int eof_found = 1;
                            for (size_t i = 0; i < 16 && i < tcb->rcv_file_sz; ++i)
                            {
                                if (tcb->rcv_file[tcb->rcv_file_sz - i - 1] != 0x7E)
                                {
                                    eof_found = 0;
                                    break;
                                }
                            }
                            if (eof_found)
                                tcb->rcv_file_sz -= 16;
                        }

                        handle_segment(tcb);
#ifdef IS_SERVER
                        // HACK: Set the remote port for every received paquet
                        tcb->remote_auth.port = seg->header.src;
                        if (tcb->head_list->head != NULL)
                            tcb->head_list->head->segment->header.dest = tcb->remote_auth.port;
#endif
                        ui_display_header(0);
                    }
                }
                else if (fds[i].fd == context->ui_event_fd && fds[i].revents & POLLIN)
                {
                    read(context->ui_event_fd, &context->ui_event_buf, sizeof (context->ui_event_buf));
                    context->ui_event_buf[0] = 1; // Reset incremented value
                    #ifdef DEBUG
                        P("Sending TCP header");
                    #endif

                    uint16_t window = tcb->rcv_head_list->head->segment->header.window;
                    ssize_t payload_len = window / PAYLOAD_WIN_DIV;
                    if (tcb->file_sz > 0 && payload_len > 0)
                    {
                        ssize_t total = tcb->file_sz - tcb->sent_sz;
                        unsigned offset = 0;
                        for (; total > 0 && offset + payload_len < window; total -= payload_len, offset += payload_len)
                        {
                            s_segment *seg = init_segment();
                            seg->header = tcb->head_list->head->segment->header; // Not supposed to be NULL
                            seg->header.payload_length = payload_len;
                            seg->header.ACK = 0; // Make sure it is not an ack segment being sent
                            seg->header.seq += payload_len; // Update sequence number
                            add_ll(tcb->head_list, seg);
                            ui_display_header(2); // 2 because we can alert user we are waiting for ACK

                            if (total < payload_len)
                                seg->header.payload_length = total;
                            seg->payload = tcb->file + tcb->sent_sz + offset;
                            send_segment(tcb->conn, tcb->head_list->head->segment);
                        }
                        tcb->sent_sz += offset; // Note: Can be greater than tcb->file_sz
                        ui_display_header(1);

                        if (tcb->sent_sz >= tcb->file_sz)
                        {
                            tcb->file_sz = 0;
                            tcb->sent_sz = 0;
                            free(tcb->file);
                            tcb->file = NULL;

                            // OSI L5 shit
                            s_segment *head_seg = tcb->head_list->head->segment; // Not supposed to be NULL
                            head_seg->header.payload_length = 16;
                            head_seg->payload = malloc(16);
                            for (int i = 0; i < 16; ++i)
                                head_seg->payload[i] = 0x7E;
                            send_segment(tcb->conn, tcb->head_list->head->segment);
                        }
                    }
                    else
                    {
                        send_segment(tcb->conn, tcb->head_list->head->segment);
                        if (tcb->state == TIME_WAIT)
                            reset_do();
                    }
                }
            }
        }
    }
}
