#include <conn/send.h>
#include <tcp/structs.h>
#include <user/print.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>

int send_segment(s_sock_conn *conn, s_segment *seg)
{
    size_t seg_len = sizeof (s_tcp_header) + seg->header.payload_length;
    char *bytes = malloc(seg_len);
    memcpy(bytes, (void *)&seg->header, sizeof (s_tcp_header));

    if (seg->header.payload_length > 0)
        memcpy(bytes + sizeof (s_tcp_header), seg->payload, seg->header.payload_length);

    int rtn = !!sendto(conn->sock, bytes, seg_len, 0, (struct sockaddr *)&conn->si, sizeof (conn->si));

    seg->header.payload_length = 0; // Security
    return rtn;
}
