#ifndef CONN_INPUT_H
# define CONN_INPUT_H

# include <tcp/tcb.h>

int check_ip_port(char *ip, char *port, s_authority *auth);

#endif // !CONN_INPUT_H
