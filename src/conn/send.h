#ifndef CONN_SEND_H
# define CONN_SEND_H

# include <tcp/tcb.h>
# include <stdint.h>

int send_segment(s_sock_conn *conn, s_segment *seg);

#endif // !CONN_SEND_H
