#ifndef TCP_TCB_H
# define TCP_TCB_H

# include <tcp/structs.h>

# define WINDOW_DEFAULT (80)
# define PAYLOAD_WIN_DIV (3)

void set_header(s_tcb *tcb, e_seg_type type);
void handle_segment(s_tcb *tcb);

#endif // !TCP_TCB_H
