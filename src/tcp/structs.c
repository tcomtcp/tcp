#include <tcp/structs.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

s_tcb *init_tcb(void)
{
    s_tcb *tcb = malloc(sizeof (s_tcb));
    tcb->conn = NULL;
    tcb->state = CLOSED;
    tcb->file_sz = 0;
    tcb->rcv_file_sz = 0;
    tcb->sent_sz = 0;
    tcb->cur_window = 0;

    tcb->head_list = init_ll();
    tcb->rcv_head_list = init_ll();

    return tcb;
}

void free_tcb(s_tcb *tcb)
{
    if (tcb->conn != NULL)
    {
        if (tcb->conn->sock > 0)
            close(tcb->conn->sock);

        free(tcb->conn);
    }

    free_ll(tcb->head_list);
    free_ll(tcb->rcv_head_list);

    free(tcb);
}

s_segment *init_segment(void)
{
    s_segment *seg = malloc(sizeof(s_segment));
    memset((char *)&seg->header, 0, sizeof (s_tcp_header));
    seg->payload = NULL;
    return seg;
}

void free_segment(s_segment *seg)
{
    free(seg);
}

s_ll *init_ll(void)
{
    s_ll *ll = malloc(sizeof(s_ll));
    ll->head = NULL;
    return ll;
}

void free_ll(s_ll *ll)
{
    s_elt *elt = ll->head;
    while (elt != NULL)
    {
        free_segment(elt->segment);

        s_elt *next = elt->next;
        free(elt);
        elt = next;
    }

    free(ll);
}

void del_after_ll(s_ll *ll, uint32_t ack_seq)
{
    if (ll->head == NULL)
        return;

    uint32_t seq = ack_seq - 1;
    s_elt *chain = NULL;

    if (ll->head->segment->header.seq == seq)
    {
        chain = ll->head;
        ll->head = NULL;
    }
    else
    {
        s_elt *elt = ll->head;
        while (elt->next != NULL)
        {
            if (elt->next->segment->header.seq == seq)
            {
                chain = elt->next;
                elt->next = NULL;
                break;
            }
            elt = elt->next;
        }
    }

    while (chain != NULL)
    {
        free_segment(chain->segment);
        s_elt *next = chain->next;
        free(chain);
        chain = next;
    }
}

static s_elt *init_elt(s_segment *seg)
{
    s_elt *elt = malloc(sizeof (s_elt));
    elt->segment = seg;
    elt->next = NULL;
    return elt;
}

void add_ll(s_ll *ll, s_segment *seg)
{
    s_elt *old_head = ll->head;
    ll->head = init_elt(seg);
    ll->head->next = old_head;
}
