#ifndef TCP_STRUCTS_H
# define TCP_STRUCTS_H

# include <stdint.h>
# include <netinet/in.h>
# include <sys/socket.h>

// TCP header
typedef enum
{
    SYN,
    ACK,
    ACK_SYN,
    CLOSING
} e_seg_type;

typedef enum
{
    CLOSED,
    SYN_SENT,
    SYN_RECVD,
    ESTABLISHED,
    LISTEN,
    ESTABLISHED_FAILED,
    CLOSE_WAIT,
    LAST_ACK,
    TIME_WAIT,
    FIN_SENT
} e_state;

# pragma pack(1)
typedef struct
{
    uint16_t src;
    uint16_t dest;

    uint32_t seq;

    uint32_t ack_seq;

    unsigned int offset: 4;
    unsigned int res: 6;
    unsigned int URG: 1;
    unsigned int ACK: 1;
    unsigned int PSH: 1;
    unsigned int RST: 1;
    unsigned int SYN: 1;
    unsigned int FIN: 1;
    uint16_t window;

    uint16_t checksum;
    uint16_t urg_ptr;
    // TODO missing OPTION; PADDING

    size_t payload_length; // HACK: This is because we do not have access to the IP packet for ip.length
} s_tcp_header;
# pragma pack(0)

// Segment linked list
typedef struct
{
    s_tcp_header header;
    char *payload;
} s_segment;

typedef struct elt s_elt;

struct elt
{
    s_segment *segment;
    s_elt *next;
};

typedef struct
{
    s_elt *head;
} s_ll;

// Authority
typedef struct
{
    uint32_t ip;
    int port;
} s_authority;

// Socket
typedef struct
{
    int sock;
    struct sockaddr_in si;
} s_sock_conn;

// Transmission Control Block
typedef struct
{
    s_authority local_auth;
    s_authority remote_auth;
    s_sock_conn *conn;

    s_ll *head_list;
    s_ll *rcv_head_list;

    e_state state;

    char *file;
    size_t file_sz;
    char *rcv_file;
    size_t rcv_file_sz;
    size_t cur_window;

    size_t sent_sz;
    char *payload;
} s_tcb;

// Allocation/free functions
s_tcb *init_tcb(void);
void free_tcb(s_tcb *tcb);
s_segment *init_segment(void);
void free_segment(s_segment *seg);
s_ll *init_ll(void);
void free_ll(s_ll *ll);
void del_after_ll(s_ll *ll, uint32_t ack_seq);
void add_ll(s_ll *ll, s_segment *seg);

#endif // !TCP_STRUCTS_H
