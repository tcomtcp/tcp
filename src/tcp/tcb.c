#include <tcp/structs.h>
#include <conn/send.h>
#include <user/print.h>
#include <user/ui.h>
#include <stdlib.h>
#include <string.h>

static void flag_syn(s_tcp_header *head)
{
    head->URG = 0;
    head->ACK = 0;
    head->PSH = 0;
    head->RST = 0;
    head->SYN = 1;
    head->FIN = 0;
}

static void flag_ack_syn(s_tcp_header *head)
{
    head->URG = 0;
    head->ACK = 1;
    head->PSH = 0;
    head->RST = 0;
    head->SYN = 1;
    head->FIN = 0;
}

static void flag_ack(s_tcp_header *head)
{
    head->URG = 0;
    head->ACK = 1;
    head->PSH = 0;
    head->RST = 0;
    head->SYN = 0;
    head->FIN = 0;
}

static void syn(s_tcp_header *head)
{
    // head->seq set before this call by yser
    head->ack_seq = 0; // TODO: first conn, what value ?
    head->offset = 0; // TODO check
    head->res = 0; // TODO check

    flag_syn(head);

    head->window = WINDOW_DEFAULT;
}

static void ack_syn(s_tcp_header *head, s_tcp_header *rcv_head)
{
    head->seq = 200; // TODO random number?
    head->ack_seq = rcv_head->seq + 1;
    head->offset = 0; // TODO check
    head->res = 0; // TODO check

    flag_ack_syn(head);

    head->window = WINDOW_DEFAULT;
}

static void ack(s_tcp_header *head, s_tcp_header *rcv_head)
{
    head->seq = rcv_head->ack_seq + 1;
    head->ack_seq = rcv_head->seq + 1;
    head->offset = 0; // TODO check
    head->res = 0; // TODO check

    flag_ack(head);
}

static void close_wait(s_tcp_header *head, s_tcp_header *rcv_head) // This method is redondant but useful for clarity
{
    head->seq = rcv_head->ack_seq + 1;
    head->ack_seq = rcv_head->seq + 1;

    flag_ack(head);
}

void set_header(s_tcb *tcb, e_seg_type type)
{
    if (tcb->head_list->head == NULL)
    {
        s_segment *seg = init_segment();
        add_ll(tcb->head_list, seg);
    }
    s_tcp_header *head = &tcb->head_list->head->segment->header;

    if (tcb->rcv_head_list->head == NULL)
    {
        s_segment *seg = init_segment();
        add_ll(tcb->rcv_head_list, seg);
    }
    s_tcp_header *rcv_head = &tcb->rcv_head_list->head->segment->header;

    head->src = tcb->local_auth.port;
    head->dest = tcb->remote_auth.port;

    if (type == SYN)
        syn(head);
    else if (type == ACK_SYN)
        ack_syn(head, rcv_head);
    else if (type == ACK)
        ack(head, rcv_head);
    else if (type == CLOSING)
        close_wait(head, rcv_head);
}

static void state_listen(s_tcb *tcb)
{
    if (tcb->rcv_head_list->head->segment->header.SYN)
    {
        tcb->state = SYN_RECVD;
        set_header(tcb, ACK_SYN);
        // TODO: Are we now in SYN_SENT? if so we need to update display before sending segment as well
        ui_display_header(0);

    }
}

static void state_syn_sent(s_tcb *tcb)
{
    s_tcp_header *head = &tcb->head_list->head->segment->header;
    s_tcp_header *rcv_head = &tcb->rcv_head_list->head->segment->header;
    // Expecting SYN ACK (2 out of 3 handshake)
    if (rcv_head->SYN == 1 && rcv_head->ACK == 1 && head->seq + 1 == rcv_head->ack_seq)
    {
        tcb->state = ESTABLISHED;
        set_header(tcb, ACK);
        print_header(tcb);
    }
    else // TODO: Specific error message?
        tcb->state = ESTABLISHED_FAILED;
}

static void state_fin_sent(s_tcb *tcb)
{
    s_tcp_header *rcv_head = &tcb->rcv_head_list->head->segment->header;
    if (rcv_head->ACK == 1)
    {
        tcb->state = TIME_WAIT;
        set_header(tcb, ACK);
        print_header(tcb);
    }
}

static void state_last_ack(s_tcb *tcb)
{
    s_tcp_header *rcv_head = &tcb->rcv_head_list->head->segment->header;
    if (rcv_head->ACK == 1)
        reset_do();
}

static void state_syn_recvd(s_tcb *tcb)
{
    s_tcp_header *head = &tcb->head_list->head->segment->header;
    s_tcp_header *rcv_head = &tcb->rcv_head_list->head->segment->header;
    if (rcv_head->ACK && head->seq + 1 == rcv_head->ack_seq && rcv_head->ACK)
        tcb->state = ESTABLISHED;
    else // TODO: Specific error message?
        tcb->state = ESTABLISHED_FAILED;
}

static void send_paquets_reverse(s_tcb *tcb, s_elt *elt) // FIXME: check if that works
{
    if (elt != NULL)
    {
        send_segment(tcb->conn, elt->segment);
        send_paquets_reverse(tcb, elt->next);
    }
}

static void state_established(s_tcb *tcb)
{
    if (tcb->rcv_head_list->head->segment->header.ACK == 1) // We are receiving a data transfer ACK
    {
        del_after_ll(tcb->head_list, tcb->rcv_head_list->head->segment->header.ack_seq);
        send_paquets_reverse(tcb, tcb->head_list->head);
        ui_lock_send(0);
    }
    else if (tcb->rcv_head_list->head->segment->header.FIN == 1) // Beggin disconnect process
    {
        tcb->state = CLOSE_WAIT;

        s_segment *seg = init_segment();
        add_ll(tcb->head_list, seg);

        set_header(tcb, CLOSING);
        ui_display_header(0);
    }
    else // We are receiving data
    {
        const char *fname = BNAME"_file.txt"; //TODO: temp var
        FILE *fp = fopen(fname, "w+");
        fwrite(tcb->rcv_file, sizeof (char), tcb->rcv_file_sz, fp);
        fclose(fp);
        //free(tcb->rcv_file); // TODO: find the right place to free this

        s_segment *seg = init_segment();
        add_ll(tcb->head_list, seg);

        seg->header.window = tcb->head_list->head->next->segment->header.window;
        set_header(tcb, ACK);
    }
}

void handle_segment(s_tcb *tcb)
{
    // TODO: Handle timeouts
    switch (tcb->state)
    {
        case LISTEN:
            state_listen(tcb);
            break;
        case SYN_SENT:
            state_syn_sent(tcb);
            break;
        case SYN_RECVD:
            state_syn_recvd(tcb);
            break;
        case ESTABLISHED:
            state_established(tcb);
            break;
        case FIN_SENT:
            state_fin_sent(tcb);
            break;
        case LAST_ACK:
            state_last_ack(tcb);
            break;
        case CLOSED:
        default:
            return;
    }
}
