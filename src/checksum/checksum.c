#include <checksum/checksum.h>

uint16_t compute(uint16_t *data, int length)
{
  uint32_t checksum = 0;

  while(length > 1) // Add each block of 16 bits
  {
    checksum += *data++;
    length   -= sizeof(uint16_t);
  }

  if(length) // Case of length is odd
    checksum += *(unsigned char*)data;

  // One's complement
  checksum = (checksum >> 16) + (checksum & 0xffff);
  checksum += (checksum >> 16);
  return (uint16_t)(~checksum);
}

uint16_t checksum(uint32_t src, uint32_t dst, s_segment segment)
{
  char buffer[65535]; // 65535 is the default max size of a window

  segment.header.checksum = 0; // Must be of 0 when computing

  // Init pseudo header
  s_pseudo_header pseudo_header;
  pseudo_header.src = src;
  pseudo_header.dst = dst;
  pseudo_header.reserved = 0;
  pseudo_header.type     = 6;
  pseudo_header.length   = htons((uint16_t)(sizeof(s_tcp_header) + strlen(segment.payload)));

  // Create buffer with continuous data
  int size = sizeof(pseudo_header);
  memcpy(buffer, &pseudo_header, sizeof(pseudo_header));
  memcpy(buffer + size, &(segment.header), sizeof(s_tcp_header));
  size += sizeof(s_tcp_header);
  memcpy(buffer + size, segment.payload, strlen(segment.payload));
  size += strlen(segment.payload);

  // Compute checksum
  return compute((uint16_t*)buffer, size);
}
