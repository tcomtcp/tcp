#ifndef CHECKSUM_H
# define CHECKSUM_H

#include <stdint.h>
#include <string.h>
#include <tcp/structs.h>

/**
 * Le Checksum est constitué en calculant le complément à 1 sur 16 bits de la somme
 * des compléments à 1 des octets de l'en-tête et des données pris deux par deux
 * (mots de 16 bits). Si le message entier contient un nombre impair d'octets, un 0 est
 * ajouté à la fin du message pour terminer le calcul du Checksum. Cet octet supplémentaire
 * n'est pas transmis. Lors du calcul du Checksum, les positions des bits attribués à
 * celui-ci sont marquées à 0.
 */

typedef struct
{
  uint32_t src;            // IP source address (32bits)
  uint32_t dst;            // IP dest address (32bits)
  char reserved;           // 8 reserved bits set to "0"
  char type;               // Protocol reference always "6" for TCP, "17" for UDP (8 bits)
  uint16_t length;         // Segment's lenght, including both header and data (16bits)
} s_pseudo_header;

uint16_t compute(uint16_t *data, int length);
uint16_t checksum(uint32_t src, uint32_t dst, s_segment segment);

#endif // !CHECKSUM_H
