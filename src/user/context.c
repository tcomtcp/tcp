#include <user/context.h>
#include <stdlib.h>
#include <sys/eventfd.h>
#include <unistd.h>

s_context *init_context(void)
{
    s_context *context = malloc(sizeof (s_context));
    context->tcb = NULL;

    context->th_running = 0;
    context->ui_event_fd = eventfd(0, 0); // We only have one thread that writes and one that reads
    context->ui_event_buf[0] = 1;

    context->field_ip = NULL;
    context->field_port = NULL;
    context->field_seq = NULL;

    context->lbl_resp = NULL;
    context->lbl_status = NULL;

    return context;
}

void free_context(s_context *context)
{
    if (context->tcb != NULL)
        free_tcb(context->tcb);

    free_context_fields(context);

    // TODO: how to free those?
    //context->lbl_resp = NULL;
    //context->lbl_status = NULL;
    //context->port_src = NULL;
    //context->port_dest = NULL;
    //context->seq = NULL;
    //context->ack_seq = NULL;
    //context->offset = NULL;
    //context->RES = NULL;
    //context->URG = NULL;
    //context->ACK = NULL;
    //context->PSH = NULL;
    //context->RST = NULL;
    //context->SYN = NULL;
    //context->FIN = NULL;

    close(context->ui_event_fd);

    free(context);
}

void free_context_fields(s_context *context)
{
    if (context->field_ip != NULL)
    {
        free(context->field_ip);
        context->field_ip = NULL;
    }
    if (context->field_port != NULL)
    {
        free(context->field_port);
        context->field_port = NULL;
    }
    if (context->field_seq != NULL)
    {
        free(context->field_seq);
        context->field_seq = NULL;
    }
}
