#ifndef USER_CONTEXT_H
# define USER_CONTEXT_H

# include <tcp/tcb.h>
# include <pthread.h>
# include <gtk/gtk.h>

typedef struct
{
    // Transmission Control Block
    s_tcb *tcb;

    // Thread control
    pthread_t tid;
    int th_running;
    int ui_event_fd;
    long long ui_event_buf[1];

    // UI data
    char *field_ip;
    char *field_port;
    char *field_seq;

    // UI display
    GtkWidget *lbl_resp;
    GtkWidget *lbl_status;

    // Headers
    GtkWidget *port_src;
    GtkWidget *port_dest;
    GtkWidget *seq;
    GtkWidget *ack_seq;
    GtkWidget *offset;
    GtkWidget *window;
    GtkWidget *checksum;
    GtkWidget *res;

    // Buttons
    GtkWidget *btn_connect;
    GtkWidget *btn_reset;
    GtkWidget *btn_tran_n;
    GtkWidget *btn_tran_f;
    GtkWidget *btn_disconnect;
    GtkWidget *btn_send;

    // Flags
    GtkWidget *URG;
    GtkWidget *ACK;
    GtkWidget *PSH;
    GtkWidget *RST;
    GtkWidget *SYN;
    GtkWidget *FIN;
} s_context;

// Allocation/free functions
s_context *init_context(void);
void free_context(s_context *context);
void free_context_fields(s_context *context);

#endif // !USER_CONTEXT_H
