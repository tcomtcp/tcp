#include <user/ui.h>
#include <user/print.h>
#include <conn/conn.h>
#include <conn/input.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <inttypes.h>

static s_context *g_context;

static void *th_conn_loop(void *arg __attribute__((unused)))
{
    g_context->th_running = 1;
    conn_loop(g_context);
    return NULL;
}

static void notify_thread(void)
{
    if (write(g_context->ui_event_fd, &g_context->ui_event_buf, sizeof (g_context->ui_event_buf)) == -1)
        PF("ERROR: Could not write to UI event file descriptor: %s", strerror(errno));
}

static void textfields_state(int on)
{
    GdkColor base_color;
    if (!on)
        gdk_color_parse("#CCCCCC", &base_color);
    else
        gdk_color_parse("#FFFFFF", &base_color);

    gtk_widget_modify_base(g_context->port_src, GTK_STATE_NORMAL, &base_color);
    gtk_widget_modify_base(g_context->port_dest, GTK_STATE_NORMAL, &base_color);
    gtk_widget_modify_base(g_context->seq, GTK_STATE_NORMAL, &base_color);
    gtk_widget_modify_base(g_context->ack_seq, GTK_STATE_NORMAL, &base_color);
    gtk_widget_modify_base(g_context->offset, GTK_STATE_NORMAL, &base_color);
    gtk_widget_modify_base(g_context->res, GTK_STATE_NORMAL, &base_color);

    // FIXME: handle checkboxes

    gtk_widget_modify_base(g_context->window, GTK_STATE_NORMAL, &base_color);
    gtk_widget_modify_base(g_context->checksum, GTK_STATE_NORMAL, &base_color);
}

static void reset_fields(void)
{
    // Disable/enable buttons
    gtk_widget_set_sensitive(g_context->btn_connect, 1);
    gtk_widget_set_sensitive(g_context->btn_tran_n, 0);
    gtk_widget_set_sensitive(g_context->btn_tran_f, 0);
    gtk_widget_set_sensitive(g_context->btn_disconnect, 0);
    gtk_widget_set_sensitive(g_context->btn_send, 0);

    // Entries
    gtk_entry_set_text(GTK_ENTRY(g_context->port_src), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->port_dest), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->seq), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->ack_seq), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->offset), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->window), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->checksum), "");
    gtk_entry_set_text(GTK_ENTRY(g_context->res), "");

    textfields_state(1);

    // Flags
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->URG), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->ACK), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->PSH), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->RST), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->SYN), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->FIN), 0);

    // Response label
    gtk_label_set_text(GTK_LABEL(g_context->lbl_resp), "(None)");

    // Buttons
    gtk_button_set_label(GTK_BUTTON(g_context->btn_send), "Send data");
}

static void connect_dial(GtkWidget *widget __attribute__((unused)), gpointer window)
{
    // Dialog creation and display
    GtkWidget *dialog = gtk_dialog_new_with_buttons("Open connection",
        window, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
#ifdef IS_SERVER
        "Listen",
#else
        "Connect",
#endif
        GTK_RESPONSE_ACCEPT, "Cancel", GTK_RESPONSE_REJECT, NULL);

    GtkWidget *table = gtk_table_new(3, 2, FALSE);
    GtkWidget *content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    gtk_container_add(GTK_CONTAINER(content_area), table);

    // Declare label for port
    GtkWidget *lbl_port = gtk_label_new("Port");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(table), lbl_port, 0, 1, 1, 2,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    GtkWidget *port = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(port), "8200");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(table), port, 1, 2, 1, 2,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

#ifndef IS_SERVER
    // Declare label for IP
    GtkWidget *lbl_dest = gtk_label_new("IP Address");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(table), lbl_dest, 0, 1, 0, 1,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    GtkWidget *dest = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(dest), "127.0.0.1");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(table), dest, 1, 2, 0, 1,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for seq no
    GtkWidget *lbl_seq = gtk_label_new("Sequence No");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(table), lbl_seq, 0, 1, 2, 3,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    GtkWidget *seq = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(seq), "0");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(table), seq, 1, 2, 2, 3,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
#endif

    // Dialog show
    gtk_widget_show_all(dialog);

    // Dialog action
    switch (gtk_dialog_run(GTK_DIALOG(dialog)))
    {
        case GTK_RESPONSE_ACCEPT:
            if (g_context->th_running)
            {
                // TODO: popup
                P("ERROR: Thread already open");
            }
            else
            {
                // Launch conn establishment
                if (g_context->tcb != NULL)
                    free_tcb(g_context->tcb);
                g_context->tcb = init_tcb();

                free_context_fields(g_context);
                g_context->field_port = strdup((char *)gtk_entry_get_text(GTK_ENTRY(port)));
#ifdef IS_SERVER
                g_context->field_ip = NULL;
                g_context->field_seq = NULL;
#else
                g_context->field_ip = strdup((char *)gtk_entry_get_text(GTK_ENTRY(dest)));
                g_context->field_seq = strdup((char *)gtk_entry_get_text(GTK_ENTRY(seq)));
#endif

                if (check_ip_port(g_context->field_ip, g_context->field_port, NULL))
                {
                    int err = pthread_create(&g_context->tid, NULL, &th_conn_loop, NULL);
                    if (err == 0)
                    {
                        #ifdef DEBUG
                            P("Connection thread created");
                        #endif
                        // Disable/enable buttons
                        gtk_widget_set_sensitive(g_context->btn_connect, 0);
                        gtk_widget_set_sensitive(g_context->btn_tran_n, 1);
                        gtk_widget_set_sensitive(g_context->btn_tran_f, 1);
                        gtk_widget_set_sensitive(g_context->btn_disconnect, 1);
                        gtk_widget_set_sensitive(g_context->btn_send, 1);
                    }
                    else
                        PF("ERROR: Cannot create thread: %s", strerror(err));
                }
                else
                    P("USER ERROR: Wrong IP address or port"); // TODO: popup
            }
            break;
        default:
            break;
    }

    gtk_widget_destroy(dialog);
}

static void disconnect(GtkWidget *widget __attribute__((unused)), gpointer window __attribute__((unused)))
{
#ifdef DEBUG
    P("Disconnection button clicked");
#endif
    if (g_context->tcb->rcv_head_list->head == NULL)
        return;

    s_tcp_header *rcv_head = &g_context->tcb->rcv_head_list->head->segment->header;

    char seq[32];
    char ack_seq[32];
    sprintf(seq, "%"PRIu32, rcv_head->ack_seq + 1);
    sprintf(ack_seq, "%"PRIu32, rcv_head->seq + 1);
    gtk_entry_set_text(GTK_ENTRY(g_context->seq), seq);
    gtk_entry_set_text(GTK_ENTRY(g_context->ack_seq), ack_seq);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->URG), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->ACK), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->PSH), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->RST), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->SYN), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->FIN), 1);
    textfields_state(1);
}

void reset_do(void)
{
    reset_fields();
    if (g_context->th_running)
    {
        pthread_kill(g_context->tid, SIGUSR1);
        g_context->th_running = 0;
    }

    if (g_context->tcb != NULL)
    {
        g_context->tcb->state = CLOSED;
        ui_display_header(1);

        free_tcb(g_context->tcb);
        g_context->tcb = NULL;
    }
}

static void reset(GtkWidget *widget __attribute__((unused)), gpointer window __attribute__((unused)))
{
#ifdef DEBUG
    P("Reset button clicked");
#endif
    reset_do();
}

static void copy_header_from_ui(void)
{
    s_segment *seg = init_segment();
    add_ll(g_context->tcb->head_list, seg);
    s_tcp_header *head = &seg->header;

    head->src = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->port_src)));
    head->dest = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->port_dest)));
    head->seq = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->seq)));
    head->ack_seq = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->ack_seq)));
    head->offset = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->offset)));
    // TODO: handle head->res

    head->URG = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->URG));
    head->ACK = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->ACK));
    head->PSH = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->PSH));
    head->RST = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->RST));
    head->SYN = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->SYN));
    head->FIN = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->FIN));

    head->window = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->window)));
    head->checksum = atoi(gtk_entry_get_text(GTK_ENTRY(g_context->checksum)));
}

static void transfer(GtkWidget *widget __attribute__((unused)), gpointer window)
{
#ifdef DEBUG
    P("Normal transfer button clicked");
#endif
    if (g_context == NULL
        || g_context->tcb == NULL
        || g_context->tcb->state != ESTABLISHED)
    {
        P("ERROR: Connection not established");
        return;
    }

    GtkWidget *file_dialog = gtk_file_chooser_dialog_new("Transfer file",
        window, GTK_FILE_CHOOSER_ACTION_OPEN,
        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
        GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

    if (gtk_dialog_run(GTK_DIALOG(file_dialog)) == GTK_RESPONSE_CANCEL)
        return;

    char *fname = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_dialog));

    struct stat st;
    stat(fname, &st);

    g_context->tcb->file = malloc(st.st_size);
    FILE *fp = fopen(fname, "r");
    fread(g_context->tcb->file, sizeof (char), st.st_size, fp);
    fclose(fp);
    g_context->tcb->file_sz = st.st_size;

    g_free(fname);
    gtk_widget_destroy(file_dialog);

    notify_thread();
}

static void transfer_urgent(GtkWidget *widget __attribute__((unused)), gpointer window __attribute__((unused)))
{
#ifdef DEBUG
    P("Urgent transfer button clicked");
#endif
}

static void send_data(GtkWidget *widget __attribute__((unused)), gpointer window __attribute__((unused)))
{
#ifdef DEBUG
    P("Send data button clicked");
#endif
    if (g_context == NULL || g_context->tcb == NULL)
    {
        P("ERROR: Connection not established");
        return;
    }

    copy_header_from_ui();
    textfields_state(0);
    g_context->tcb->file_sz = 0;

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->FIN)))
        g_context->tcb->state = FIN_SENT;

    if (g_context->tcb->state == CLOSE_WAIT && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->ACK))) //End of disconnect for client
    {
        g_context->tcb->state = LAST_ACK;
        ui_display_header(1);
    }

    if (g_context->tcb->state == TIME_WAIT && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g_context->ACK))) //End of disconnect for client
    {
        notify_thread();
        reset_fields();
        return;
    }

    notify_thread();
}

GtkWidget* ui_init(s_context *context, int argc, char *argv[])
{
    // Save context
    g_context = context;

    // Init gtk
    gtk_init(&argc, &argv);

    // Create and set option for the window
    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "TCP "BNAME);
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

    // Table to organize
    GtkWidget *table_main = gtk_table_new(5, 2, FALSE);
    gtk_container_add(GTK_CONTAINER(window), table_main);

    // Buttons
    g_context->btn_connect = gtk_button_new_with_label("Open connection");
    g_context->btn_reset = gtk_button_new_with_label("Reset");
    g_context->btn_tran_n = gtk_button_new_with_label("Normal Data Transfer");
    g_context->btn_tran_f = gtk_button_new_with_label("Urgent Data Transfer");
    g_context->btn_disconnect = gtk_button_new_with_label("Disconnect");

    // Labels
    g_context->lbl_resp = gtk_label_new("(None)");
    g_context->lbl_status = gtk_label_new("Closed");
    GtkWidget *vbbox =  gtk_vbutton_box_new();

    // Expanders
    GtkWidget *exp_co = gtk_expander_new("1. Connection");
    gtk_expander_set_expanded(GTK_EXPANDER(exp_co), 1);
    GtkWidget *exp_tran = gtk_expander_new("2. Actions");
    gtk_expander_set_expanded(GTK_EXPANDER(exp_tran), 1);
    GtkWidget *exp_remote_seg = gtk_expander_new("3. Segment");
#ifdef IS_SERVER
    GtkWidget *exp_resp = gtk_expander_new("4. Segment received from client");
#else
    GtkWidget *exp_resp = gtk_expander_new("4. Segment received from server");
#endif
    gtk_expander_set_expanded(GTK_EXPANDER(exp_resp), 1);
    gtk_expander_set_expanded(GTK_EXPANDER(exp_remote_seg), 1);

    // SEGMENT HEADER VALUES
    // This next section displays the header values for the next
    // segment that will be sent. This enables user to either validate
    // and send or make some modifications and send

    GtkWidget *t_send_seg = gtk_table_new(12, 2, FALSE);
    gtk_container_add(GTK_CONTAINER(exp_remote_seg), t_send_seg);

    // Declare label for PORT SOURCE
    GtkWidget *lbl_port_src = gtk_label_new("Port Source");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_port_src, 0, 1, 0, 1,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->port_src = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->port_src), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->port_src, 1, 2, 0, 1,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for PORT DEST
    GtkWidget *lbl_port_dest = gtk_label_new("Port Dest");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_port_dest, 0, 1, 1, 2,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->port_dest = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->port_dest), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->port_dest, 1, 2, 1, 2,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for SEQUENCE NUMBER
    GtkWidget *lbl_seq = gtk_label_new("Sequence No");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_seq, 0, 1, 2, 3,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->seq = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->seq), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->seq, 1, 2, 2, 3,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for ACKNOWLEDGEMENT NUMBER
    GtkWidget *lbl_ack_seq = gtk_label_new("Ack No");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_ack_seq, 0, 1, 3, 4,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->ack_seq = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->ack_seq), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->ack_seq, 1, 2, 3, 4,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for OFFSET
    GtkWidget *lbl_offset = gtk_label_new("Offset");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_offset, 0, 1, 4, 5,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->offset = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->offset), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->offset, 1, 2, 4, 5,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for Reserved
    GtkWidget *lbl_res = gtk_label_new("Reserved");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_res, 0, 1, 5, 6,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->res = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->res), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->res, 1, 2, 5, 6,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // FLAGS
    // URG
    g_context->URG = gtk_check_button_new_with_label ("URG");
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->URG, 0, 1, 6, 7,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // ACK
    g_context->ACK = gtk_check_button_new_with_label ("ACK");
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->ACK, 1, 2, 6, 7,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // PSH
    g_context->PSH = gtk_check_button_new_with_label ("PSH");
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->PSH, 0, 1, 7, 8,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // RST
    g_context->RST = gtk_check_button_new_with_label ("RST");
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->RST, 1, 2, 7, 8,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // SYN
    g_context->SYN = gtk_check_button_new_with_label ("SYN");
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->SYN, 0, 1, 8, 9,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // FIN
    g_context->FIN = gtk_check_button_new_with_label ("FIN");
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->FIN, 1, 2, 8, 9,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for WINDOW
    GtkWidget *lbl_window = gtk_label_new("Window");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_window, 0, 1, 9, 10,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->window = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->window), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->window, 1, 2, 9, 10,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Declare label for CHECKSUM
    GtkWidget *lbl_checksum = gtk_label_new("Checksum");
    // Attach label to table
    gtk_table_attach(GTK_TABLE(t_send_seg), lbl_checksum, 0, 1, 10, 11,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    // Create text field
    g_context->checksum = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(g_context->checksum), "");
    // Attach field to table
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->checksum, 1, 2, 10, 11,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // END SEGMENT HEADER VALUES

    g_context->btn_send = gtk_button_new_with_label("Send data");
    g_signal_connect(G_OBJECT(g_context->btn_send), "clicked",
        G_CALLBACK(send_data), NULL);
    gtk_table_attach(GTK_TABLE(t_send_seg), g_context->btn_send, 0, 1, 11, 12,
        GTK_SHRINK, GTK_SHRINK, 5, 5);

    // Expanders attachement
    gtk_container_add(GTK_CONTAINER(exp_co), g_context->btn_connect);
    gtk_container_add(GTK_CONTAINER(vbbox), g_context->btn_tran_n);
    gtk_container_add(GTK_CONTAINER(vbbox), g_context->btn_tran_f);
    gtk_container_add(GTK_CONTAINER(vbbox), g_context->btn_disconnect);
    gtk_container_add(GTK_CONTAINER(exp_tran), vbbox);
    gtk_container_add(GTK_CONTAINER(exp_resp), g_context->lbl_resp);
    gtk_misc_set_alignment(GTK_MISC(g_context->lbl_resp), 0, 0);

    // Buttons signals
    g_signal_connect(G_OBJECT(g_context->btn_connect), "clicked",
        G_CALLBACK(connect_dial), NULL);
    g_signal_connect(G_OBJECT(g_context->btn_reset), "clicked",
        G_CALLBACK(reset), NULL);
    g_signal_connect(G_OBJECT(g_context->btn_tran_n), "clicked",
        G_CALLBACK(transfer), NULL);
    g_signal_connect(G_OBJECT(g_context->btn_tran_f), "clicked",
        G_CALLBACK(transfer_urgent), NULL);
    g_signal_connect(G_OBJECT(g_context->btn_disconnect), "clicked",
        G_CALLBACK(disconnect), NULL);

    // Left
    gtk_table_attach(GTK_TABLE(table_main), exp_co, 0, 1, 0, 1,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_table_attach(GTK_TABLE(table_main), exp_tran, 0, 1, 1, 2,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_table_attach(GTK_TABLE(table_main), exp_remote_seg, 0, 1, 2, 3,
        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // Right
    gtk_table_attach(GTK_TABLE(table_main), g_context->lbl_status, 1, 2, 0, 1,
        GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_table_attach(GTK_TABLE(table_main), g_context->btn_reset, 1, 2, 4, 6,
        GTK_SHRINK, GTK_SHRINK, 5, 5);
    gtk_table_attach(GTK_TABLE(table_main), exp_resp, 1, 2, 2, 5,
        GTK_SHRINK, GTK_FILL, 5, 5);

    // Show the window and activate its destroy signal
    gtk_widget_show_all(window);
    g_signal_connect(G_OBJECT(window), "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

    // Disable buttons
    gtk_widget_set_sensitive(g_context->btn_tran_n, 0);
    gtk_widget_set_sensitive(g_context->btn_tran_f, 0);
    gtk_widget_set_sensitive(g_context->btn_disconnect, 0);
    gtk_widget_set_sensitive(g_context->btn_send, 0);

    return window;
}

void ui_loop(void)
{
    gtk_main();
}

static char *get_seq_label(s_tcp_header *head)
{
    const char *fmt = "Source: %d\nDest: %d\nSequence No: %d\n"
        "Ack Number: %d\nOffset: %d\nRes: %d\nURG: %d\nACK: %d\n"
        "PSH: %d\nRST: %d\nSYN: %d\nFIN: %d\nWindow: %d\nChecksum: %d\n\n"
        "Data for current window: %d";
    char *str = malloc(snprintf(NULL, 0, fmt,
        head->src, head->dest, head->seq, head->ack_seq, head->offset,
        head->res, head->URG, head->ACK, head->PSH, head->RST, head->SYN,
        head->FIN, head->window, head->checksum, g_context->tcb->cur_window) + 1);
    sprintf(str, fmt, head->src, head->dest, head->seq, head->ack_seq,
        head->offset, head->res, head->URG, head->ACK, head->PSH, head->RST,
        head->SYN, head->FIN, head->window, head->checksum, g_context->tcb->cur_window);
    return str;
}

static char *status_to_str(e_state status)
{
    s_tcb *tcb = g_context->tcb;
    switch (status)
    {
        case CLOSED:
            return strdup("Closed");
        case SYN_SENT:
            return strdup("Syn Sent");
        case SYN_RECVD:
            return strdup("Syn Received");
        case ESTABLISHED:
            if (tcb->sent_sz > 0)
            {
                const char *fmt = "Established (file sent: %lu/%lu)";
                size_t sent_sz = tcb->sent_sz;
                if (tcb->file_sz < sent_sz)
                    sent_sz = tcb->file_sz;
                char *str = malloc(snprintf(NULL, 0, fmt, sent_sz, tcb->file_sz) + 1);
                sprintf(str, fmt, sent_sz, tcb->file_sz);
                return str;
            }
            return strdup("Established");
        case LISTEN:
            return strdup("Listen");
        case CLOSE_WAIT:
            return strdup("Close Wait");
        case LAST_ACK:
            return strdup("Last ACK");
        case TIME_WAIT:
            return strdup("Time Wait");
        case FIN_SENT:
            return strdup("Established"); // HACK
        default:
            return strdup("Failed");
    }
}

void ui_display_header(int state_only)
{
    // Status
    char *str_status = status_to_str(g_context->tcb->state);
    gtk_label_set_text(GTK_LABEL(g_context->lbl_status), str_status);
    free(str_status);

    if (!state_only || state_only == 2)
    {
        s_elt *h = g_context->tcb->head_list->head;
        if (h != NULL)
        {
            s_tcp_header *head = &h->segment->header;

            // Headers
            char src[32];
            char dest[32];
            char seq[32];
            char ack_seq[32];
            // TODO: offset
            // TODO: res
            char window[32];
            char checksum[32];
            sprintf(src, "%"PRIu16, head->src);
            sprintf(dest, "%"PRIu16, head->dest);
            sprintf(seq, "%"PRIu32, head->seq);
            sprintf(ack_seq, "%"PRIu32, head->ack_seq);
            sprintf(window, "%"PRIu16, head->window);
            sprintf(checksum, "%"PRIu16, head->checksum);

            gtk_entry_set_text(GTK_ENTRY(g_context->port_src), src);
            gtk_entry_set_text(GTK_ENTRY(g_context->port_dest), dest);
            gtk_entry_set_text(GTK_ENTRY(g_context->seq), seq);
            gtk_entry_set_text(GTK_ENTRY(g_context->ack_seq), ack_seq);

            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->URG), head->URG);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->ACK), head->ACK);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->PSH), head->PSH);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->RST), head->RST);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->SYN), head->SYN);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(g_context->FIN), head->FIN);

            gtk_entry_set_text(GTK_ENTRY(g_context->window), window);
            gtk_entry_set_text(GTK_ENTRY(g_context->checksum), checksum);

            if (state_only == 2)
            {
                ui_lock_send(1);
            }
        }

        // Remote segment received
        s_elt *rcv_h = g_context->tcb->rcv_head_list->head;
        if (rcv_h != NULL)
        {
            char *str_rcv_h = get_seq_label(&rcv_h->segment->header);
            gtk_label_set_text(GTK_LABEL(g_context->lbl_resp), str_rcv_h);
            free(str_rcv_h);
        }

        textfields_state(1);
    }
}

void ui_lock_send(int needs_lock)
{
    if (needs_lock)
    {
        gtk_button_set_label(GTK_BUTTON(g_context->btn_send), "Waiting...");
        gtk_widget_set_sensitive(g_context->btn_send, 0);
        gtk_widget_set_sensitive(g_context->btn_tran_n, 0);
        gtk_widget_set_sensitive(g_context->btn_tran_f, 0);
    }
    else
    {
        gtk_button_set_label(GTK_BUTTON(g_context->btn_send), "Send data");
        gtk_widget_set_sensitive(g_context->btn_send, 1);
        gtk_widget_set_sensitive(g_context->btn_tran_n, 1);
        gtk_widget_set_sensitive(g_context->btn_tran_f, 1);
    }
}
