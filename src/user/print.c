#include <user/print.h>
#include <stdlib.h>
#include <arpa/inet.h>

void print_header(s_tcb *tcb)
{
    if (tcb->head_list->head != NULL)
    {
        s_tcp_header *head = &tcb->head_list->head->segment->header;

        FILE *fp = fopen("segments.log", "a+");

        FPF(fp, "Source: %d", head->src);
        FPF(fp, "Dest: %d", head->dest);
        FPF(fp, "Sequence No: %d", head->seq);
        FPF(fp, "Ack Number: %d", head->ack_seq);
        FPF(fp, "Offset: %d", head->offset);
        FPF(fp, "Res: %d", head->res);
        FPF(fp, "URG: %d", head->URG);
        FPF(fp, "ACK: %d", head->ACK);
        FPF(fp, "PSH: %d", head->PSH);
        FPF(fp, "RST: %d", head->RST);
        FPF(fp, "SYN: %d", head->SYN);
        FPF(fp, "FIN: %d", head->FIN);
        FPF(fp, "Window: %d", head->window);
        FPF(fp, "Checksum: %d", head->checksum);
        FP(fp, "---");

        fclose(fp);
    }
}

char *get_str_ip(s_authority auth)
{
    char *ip = malloc(sizeof (struct in_addr));
    inet_ntop(AF_INET, &auth.ip, ip, INET_ADDRSTRLEN);
    return ip;
}
