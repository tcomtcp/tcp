#ifndef USER_PRINT_H
# define USER_PRINT_H

# include <tcp/structs.h>
# include <stdio.h>

# define P(STR) printf("["BTYPE"] "STR"\n")
# define PF(STR, ...) printf("["BTYPE"] "STR"\n", __VA_ARGS__)
# define FP(F, STR) fprintf(F, "["BTYPE"] "STR"\n")
# define FPF(F, STR, ...) fprintf(F, "["BTYPE"] "STR"\n", __VA_ARGS__)

void print_header(s_tcb *tcb);
char *get_str_ip(s_authority auth);

#endif // !USER_PRINT_H
