#ifndef USER_UI_H
# define USER_UI_H

# ifndef _BSD_SOURCE
#  define _BSD_SOURCE
# endif // !_BSD_SOURCE

# ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 199506L
# endif // !_POSIX_C_SOURCE

# include <user/context.h>
# include <gtk/gtk.h>

GtkWidget* ui_init(s_context *context, int argc, char *argv[]);
void ui_loop(void);
void ui_display_header(int state_only);
void ui_lock_send(int needs_lock);
void reset_do(void);
# endif // !USER_UI_H
