#include "utils.h"
#include "../tcp/services/tcb.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

void print_header(s_tcb *tcb)
{
    s_tcp_header *head = &tcb->head;

    FILE *fp = fopen("segments.log", "a+");

    FPF(fp, "Source: %d", head->src);
    FPF(fp, "Dest: %d", head->dest);
    FPF(fp, "Sequence Number: %d", head->seq);
    FPF(fp, "Ack Number: %d", head->ack_seq);
    FPF(fp, "Offset: %d", head->offset);
    FPF(fp, "Res: %d", head->res);
    FPF(fp, "URG: %d", head->URG);
    FPF(fp, "ACK: %d", head->ACK);
    FPF(fp, "PSH: %d", head->PSH);
    FPF(fp, "RST: %d", head->RST);
    FPF(fp, "SYN: %d", head->SYN);
    FPF(fp, "FIN: %d", head->FIN);
    FP(fp, "---");

    fclose(fp);

    printf("\tSource: %d\n", head->src);
    printf("\tDest: %d\n", head->dest);
    printf("\tSequence Number: %d\n", head->seq);
    printf("\tAck Number: %d\n", head->ack_seq);
    printf("\tOffset: %d\n", head->offset);
    printf("\tRes: %d\n", head->res);
    printf("\tURG: %d\n", head->URG);
    printf("\tACK: %d\n", head->ACK);
    printf("\tPSH: %d\n", head->PSH);
    printf("\tRST: %d\n", head->RST);
    printf("\tSYN: %d\n", head->SYN);
    printf("\tFIN: %d\n", head->FIN);
}

int send_segment(s_sock_conn *conn, void *buf, size_t len)
{
    return !!sendto(conn->sock, buf, len, 0, (struct sockaddr *)&conn->si, sizeof(conn->si));
}

char *get_str_ip(s_authority auth)
{
    char *ip = malloc(sizeof(struct in6_addr));
    inet_ntop(AF_INET, &auth.ip, ip, INET_ADDRSTRLEN);

    return ip;
}

s_authority check_ip_port(char *ip, char *port)
{
    //char ip_addr[sizeof(struct in6_addr)];
    struct sockaddr_in sa;
    s_authority auth = (s_authority) { .ip = 0, .port = 0 };

    // IP
    if (ip == NULL)
        auth.ip = INADDR_ANY;
    else
    {
        if (inet_pton(AF_INET, ip, &sa.sin_addr) <= 0)
        {
#ifdef DEBUG
            P("ERROR: Invalid IP address");
            exit(0);
#endif
            return auth;
        }
        auth.ip = sa.sin_addr.s_addr;
        //inet_ntop(AF_INET, &auth.ip, ip_addr, INET_ADDRSTRLEN);
    }

    // PORT
    //auth.port = strtol(port, NULL, 16);
    auth.port = atoi(port);
    if (auth.port < 1 || auth.port > 65535)
    {
#ifdef DEBUG
        P("ERROR: Invalid PORT number\n");
        exit(0);
#endif
        auth.port = 0;
        return auth;
    }

    return auth;
}
