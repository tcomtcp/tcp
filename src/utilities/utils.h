#ifndef UTILS_H
# define UTILS_H

# include "../tcp/services/tcb.h"

# include <stdint.h>

# include <stdio.h>
# define P(STR) printf("["BIN_TYPE"] "STR"\n")
# define PF(STR, ...) printf("["BIN_TYPE"] "STR"\n", __VA_ARGS__)
# define FP(F, STR) fprintf(F, "["BIN_TYPE"] "STR"\n")
# define FPF(F, STR, ...) fprintf(F, "["BIN_TYPE"] "STR"\n", __VA_ARGS__)

void print_header(s_tcb *tcb);
int send_segment(s_sock_conn *conn, void *buf, size_t len);

// returns string IP
char *get_str_ip(s_authority auth);

// Returns a struct with valid IP and PORT
// IF either are false, the value will be 0
s_authority check_ip_port(char *ip, char *port);

#endif // !UTILS_H
