#include <user/context.h>
#include <user/ui.h>
#include <signal.h>

static s_context *g_context = NULL;

static void signal_handler(int sig)
{
    if (sig != SIGCHLD && sig != SIGCONT && g_context != NULL)
        free_context(g_context);
}

int main(int argc, char *argv[])
{
    signal(SIGINT, signal_handler);

    g_context = init_context();

    ui_init(g_context, argc, argv);
    ui_loop();

    return 0;
}
