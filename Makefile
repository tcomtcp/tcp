CC = gcc
CFLAGS = -std=c99 -Wall -Werror -Wextra -pedantic `pkg-config --cflags --libs gtk+-2.0`
INC = -I./src
LDLIBS = -pthread `pkg-config --libs gtk+-2.0`
DEBUG = -g3 -D DEBUG
AOUT_SRV = server
AOUT_CLT = client

SRC = main.c \
      tcp/structs.c tcp/tcb.c \
      conn/conn.c conn/input.c conn/send.c \
      user/context.c user/ui.c user/print.c \
			checksum/checksum.c

DIRS = tcp conn user checksum

SRCDIR = src/
BLDDIR = build/
BLDDIR_SRV = $(BLDDIR)srv/
BLDDIR_CLT = $(BLDDIR)clt/
OBJ_SRV := $(addprefix $(BLDDIR_SRV), $(SRC:.c=.o))
OBJ_CLT := $(addprefix $(BLDDIR_CLT), $(SRC:.c=.o))

all: $(AOUT_SRV) $(AOUT_CLT)

$(AOUT_SRV): CFLAGS += -DBTYPE=\"SRV\" -DBNAME=\"Server\" -DIS_SERVER
$(AOUT_SRV): $(OBJ_SRV)
	$(CC) -o $@ $^ $(LDLIBS)

$(AOUT_CLT): CFLAGS += -DBTYPE=\"CLT\" -DBNAME=\"Client\"
$(AOUT_CLT): $(OBJ_CLT)
	$(CC) -o $@ $^ $(LDLIBS)

$(BLDDIR_SRV):
	mkdir -p $(addprefix $(BLDDIR_SRV), $(DIRS))

$(BLDDIR_CLT):
	mkdir -p $(addprefix $(BLDDIR_CLT), $(DIRS))

$(BLDDIR_SRV)%.o: $(SRCDIR)%.c | $(BLDDIR_SRV)
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@

$(BLDDIR_CLT)%.o: $(SRCDIR)%.c | $(BLDDIR_CLT)
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@

debug: CFLAGS += $(DEBUG)
debug: all

run: debug
	./$(AOUT_SRV) & ./$(AOUT_CLT)

# FIXME trying to make some simple rules to compile and run either the client or server only
run-client: CFLAGS += $(DEBUG)
run-client: client
	rm -f segments.log
	./$(AOUT_CLT)

run-server: CFLAGS += $(DEBUG)
run-server: server
	rm -f segments.log
	./$(AOUT_SRV)
	killall $(AOUT_SRV)

clean:
	$(RM) $(OBJ_SRV) $(OBJ_CLT)
	$(RM) $(AOUT_SRV) $(AOUT_CLT)

distclean: clean
	$(RM) -r $(BLDDIR)
